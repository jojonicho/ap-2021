package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AgileAdventurer extends Adventurer {
    @Override
    public String getAlias() {
        return "Agile";
    }

    public AgileAdventurer() {
        setAttackBehavior(new AttackWithGun());
        setDefenseBehavior(new DefendWithBarrier());
    }

    //ToDo: Complete me
}
