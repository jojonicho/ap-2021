package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {
    @Override
    public String attack() {
        return "Pedang go brr";
    }

    @Override
    public String getType() {
        return "Pedang";
    }
    //ToDo: Complete me
}
