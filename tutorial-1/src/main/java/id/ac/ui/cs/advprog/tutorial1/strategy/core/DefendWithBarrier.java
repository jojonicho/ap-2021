package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
    @Override
    public String defend() {
        return "Penghadang gets brr";
    }

    @Override
    public String getType() {
        return "Penghadang";
    }
    //ToDo: Complete me
}
