package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {


    public AgileAdventurer(Guild guild) {
        this.name = "Agile";
        this.guild = guild;
        this.guild.add(this);
    }

    @Override
    public void update() {
        String questType = guild.getQuestType();
        if(questType.equalsIgnoreCase("D") || questType.equalsIgnoreCase("R")) {
            this.getQuests().add(guild.getQuest());
        }
    }

}
