package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class KnightAdventurer extends Adventurer {


    public KnightAdventurer(Guild guild) {
        this.name = "Knight";
        this.guild = guild;
        this.guild.add(this);
    }

    @Override
    public void update() {
        String questType = guild.getQuestType();
        // basically semua, tapi in case ada quest type baru
        if(questType.equalsIgnoreCase("D") || questType.equalsIgnoreCase("R") || questType.equalsIgnoreCase("E")) {
            this.getQuests().add(guild.getQuest());
        }
    }

    //ToDo: Complete Me
}
