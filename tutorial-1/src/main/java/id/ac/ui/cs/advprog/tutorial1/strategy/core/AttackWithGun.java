package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {
    @Override
    public String attack() {
        return "Senjata Api go brr";
    }

    @Override
    public String getType() {
        return "Senjata Api";
    }
    //ToDo: Complete me
}
