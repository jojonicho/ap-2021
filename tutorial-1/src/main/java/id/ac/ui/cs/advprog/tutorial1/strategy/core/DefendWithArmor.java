package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
    @Override
    public String defend() {
        return "Armor gets brr";
    }

    @Override
    public String getType() {
        return "Armor";
    }
    //ToDo: Complete me
}
