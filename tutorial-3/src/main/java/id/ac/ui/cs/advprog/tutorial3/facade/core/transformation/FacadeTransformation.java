package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.adapter.TranslatorAdapter;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.CodexTranslator;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

import java.util.ArrayList;
import java.util.List;

public class FacadeTransformation implements Transformation {
    private List<Transformation> transformationList = new ArrayList<>();

    public FacadeTransformation() {
        addTransformation(new TranslatorAdapter(new CodexTranslator()));
        addTransformation(new CelestialTransformation());
        addTransformation(new AbyssalTransformation());
        addTransformation(new CaesarTransformation());
    }

    @Override
    public Spell encode(Spell spell) {
        for (Transformation transformation: transformationList) {
            spell = transformation.encode(spell);
        }
        return spell;
    }

    @Override
    public Spell decode(Spell spell) {
        for (int i=transformationList.size()-1; i>=0; i--) {
            spell = transformationList.get(i).decode(spell);
        }
        return spell;
    }

    public List<Transformation> getTransformations() {
        return this.transformationList;
    }

    // kayak diskusi forum, masukin list
    public void addTransformation(Transformation transformation) {
        this.transformationList.add(transformation);
    }
}
