package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.IonicBow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

// TODO: Complete me. Modify this class as you see fit~
@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)

    @Autowired
    private LogRepository logRepository;

    @Autowired
    private WeaponRepository weaponRepository;

    @Autowired
    private BowRepository bowRepository;

    @Autowired
    private SpellbookRepository spellbookRepository;

    // TODO: implement me
    @Override
    public List<Weapon> findAll() {
        List<Weapon> weaponList = weaponRepository.findAll();
        List<Bow> bowList = bowRepository.findAll();
        List<Spellbook> spellbookList = spellbookRepository.findAll();
        for(Bow bow: bowList) {
            weaponList.add(new BowAdapter(bow));
        }
        for(Spellbook spellbook: spellbookList) {
            weaponList.add(new SpellbookAdapter(spellbook));
        }
        return weaponList;
    }

//    // TODO: implement me
//    @Override
//    public void attackWithWeapon(String weaponName, int attackType) {
//        Weapon weapon = weaponRepository.findByAlias(weaponName);
//        String attack = null;
//
//        if (null != weapon) {
//            if (attackType == 0) {
//                attack = weapon.normalAttack();
//            } else if (attackType == 1) {
//                attack = weapon.chargedAttack();
//            }
//        }
//        if (null == weapon) {
//            Bow bow = bowRepository.findByAlias(weaponName);
//
//            if (attackType == 0) {
//
//            } else if (attackType == 1) {
//
//            }
//        }
//        if (null == weapon) {
//            weapon = new SpellbookAdapter(spellbookRepository.findByAlias(weaponName));
//        }
//
//
//        logRepository.addLog(attack);
//        weaponRepository.save(weapon);
//    }

    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon weapon = weaponRepository.findByAlias(weaponName);
        Bow bow = bowRepository.findByAlias(weaponName);
        Spellbook spellbook = spellbookRepository.findByAlias(weaponName);
        if (bow != null) {
            weapon = new BowAdapter(bow);
        } else if (spellbook != null) {
            weapon = new SpellbookAdapter(spellbook);
        }

        if (null == weapon) {
            return;
        }

        String attack = null;
        if (attackType == 0) {
            attack = weapon.normalAttack();
        } else if (attackType == 1) {
            attack = weapon.chargedAttack();
        }

        if(null == attack) {
            return;
        }

        logRepository.addLog(attack);
        weaponRepository.save(weapon);
    }

    // TODO: implement me
    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}
