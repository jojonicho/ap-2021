package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public class CaesarTransformation implements Transformation{
    private int shift;

    public CaesarTransformation(int shift) {
        this.shift = shift;
    }

    public CaesarTransformation() {
        this.shift = 1;
    }

    public Spell encode(Spell spell) {
        return process(spell, true);
    }

    public Spell decode(Spell spell) {
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean isEncode) {
        StringBuilder sb = new StringBuilder();

        int shiftSign = isEncode ? shift : -shift;

        for (char c : spell.getText().toCharArray()) {
            sb.append(getShiftedChar(c, shiftSign));
        }

        return new Spell(sb.toString(), spell.getCodex());
    }

    private char getShiftedChar(char target, int shift) {
        return (char) (target + shift);
    }
}