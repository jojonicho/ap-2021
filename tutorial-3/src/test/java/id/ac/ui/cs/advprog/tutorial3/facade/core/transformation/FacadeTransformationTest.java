package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class FacadeTransformationTest {
    private Class<?> facadeClass;

    @BeforeEach
    public void setup() throws Exception {
        facadeClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.FacadeTransformation");
    }

    @Test
    public void testFacadeHasEncodeMethod() throws Exception {
        Method translate = facadeClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testFacadeEncodesCorrectly() throws Exception {
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "E`N$\"KKC\"Hy|Y*c>>@=E)'rEE`efoDLWGE*[&&x}AroECOG<OT&0";

        Spell result = new FacadeTransformation().encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testFacadeHasDecodeMethod() throws Exception {
        Method translate = facadeClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testFacadeDecodesCorrectly() throws Exception {
        String text = "E`N$\"KKC\"Hy|Y*c>>@=E)'rEE`efoDLWGE*[&&x}AroECOG<OT&0";
        Codex codex = RunicCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Safira and I went to a blacksmith to forge our sword";

        Spell result = new FacadeTransformation().decode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testFacadeListContainsSuccess() {
        List<Transformation> transformationList = new FacadeTransformation().getTransformations();

        assertTrue(transformationList.stream()
                .anyMatch(transformation -> transformation instanceof AbyssalTransformation));
        assertTrue(transformationList.stream()
                .anyMatch(transformation -> transformation instanceof CelestialTransformation));
        assertTrue(transformationList.stream()
                .anyMatch(transformation -> transformation instanceof CaesarTransformation));
    }

    @Test
    public void testFacadeListContainsFail() {
        List<Transformation> transformationList = new FacadeTransformation().getTransformations();

        assertFalse(transformationList.stream()
                .anyMatch(transformation -> transformation instanceof FacadeTransformation));
    }
}
